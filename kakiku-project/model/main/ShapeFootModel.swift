//
//  ShapeFootModel.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 19/07/22.
//

import Foundation

struct ShapeFootModel: Identifiable {
    let id: Int
    let name: ShapeFoot
    let imageName: String
}

enum ShapeFoot: String {
    case step1
    case step2
    case step3
}

var dummyShapeFoot: [ShapeFootModel] = [
    .init(id: 0, name: ShapeFoot.step1, imageName: "tutorial-step1"),
    .init(id: 1, name: ShapeFoot.step2, imageName: "tutorial-step2"),
    .init(id: 2, name: ShapeFoot.step3, imageName: "tutorial-step3")
]
