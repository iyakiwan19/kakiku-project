//
//  TypeFootModel.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 26/06/22.
//

import Foundation
import SwiftUI

struct TypeFootModel: Identifiable {
    let id: Int
    let name: TypeFoot
    let imageName: String
    let description: String
    var isSelected: Bool
}

enum TypeFoot: String {
    case normal = "Normal"
    case underpronation = "Underpronation"
    case overpronation = "Overpronation"
}
