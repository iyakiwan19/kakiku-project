//
//  MeasurementViewModel.swift
//  kakiku-project
//
//  Created by Taufiq Ichwanusofa on 16/07/22.
//

import Foundation
import SwiftUI

class MeasurementViewModel: ObservableObject {
    @Published var attempted: Bool = false
    @Published var preventDismissal: Bool = true
    
    func recapture() {
        self.preventDismissal = false
    }
}
