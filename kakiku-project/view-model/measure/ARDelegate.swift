//
//  ARDelegate.swift
//  kakiku-project
//
//  Created by Gualtiero Frigerio on 18/05/21.
//  Edited by Ricky Suprayudi on 27/06/22.
//

import Foundation
import ARKit
import UIKit

class ARDelegate: NSObject, ARSCNViewDelegate, ObservableObject, ARCoachingOverlayViewDelegate {   // view-model/measure
    @Published var message:String = "Aim to floor"
    @Published var showAlert = false
    @Published var sizeFoot: Float = 28.0
    
    @Published var undoButtonDisabled = false
    @Published var okButtonDisabled = true
    @Published var measureButtonDisabled = false
    @Published var speechDisabled = true
    @Published var isFeetDrawn = false
    @Published var isInsideBox = false
    @Published var isCoachingActive = false
    
    var planeWidth = CGFloat()
    var planeHeight = CGFloat()
    var isPlaneAnchored = false
    
    var speech = Speech()
    
    var planes = [SCNNode()]
    var planeNode = SCNNode()

    var dotNode = SCNNode()
    var virtualNode = SCNNode()
    var rulerNode = SCNNode()
    var footNode = SCNNode()
    var borderNode = SCNNode()
    var sliderNode = SCNNode()
    var textNode = SCNNode()
    var curtainNode = [SCNNode](repeating: SCNNode(), count: 4)
    
    var newRotation : CGFloat = 0.0
    var oldRotation : CGFloat = 0.0
    
    var direction : Float = 0.0
    let configuration = ARWorldTrackingConfiguration()
    var panGesture = UIPanGestureRecognizer()
    var tapGesture = UITapGestureRecognizer()
    var curtainAdded = false

    func setARView(_ arView: ARSCNView) {
        guard ARWorldTrackingConfiguration.isSupported else { return }
        self.arView = arView
        configuration.planeDetection = .horizontal
        configuration.isLightEstimationEnabled = true
//        arView.debugOptions = ARSCNDebugOptions.showFeaturePoints
//        arView.showsStatistics = true
        arView.session.run(configuration)
        
        arView.delegate = self
        arView.scene = SCNScene()

        let coachingOverlay = ARCoachingOverlayView()
        
        coachingOverlay.delegate = self
        coachingOverlay.session = arView.session
        coachingOverlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // MARK: CoachingGoal
        coachingOverlay.goal = .anyPlane
        arView.addSubview(coachingOverlay)
                
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnARView))
        tapGesture.numberOfTapsRequired = 2
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(panOnARView))
        arView.addGestureRecognizer(panGesture)
        arView.addGestureRecognizer(tapGesture)
    }
    
    func pauseAR() {
        guard let arView = arView else { return }
        arView.session.pause()
    }
    
    func resumeAR() {
        guard let arView = arView else { return }
        arView.session.run(configuration)
    }
    
    @objc func tapOnARView(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            for curtains in curtainNode {
                curtains.isHidden.toggle()
            }
        }
    }
    
    @objc func panOnARView(sender: UIPanGestureRecognizer) {
        guard let arView = arView else { return }
        print("panOnARView(): start")
        switch sender.state {
        case .began:
            print("panOnARView(): began")
        case .changed:
            print("panOnARView(): changed")
            newRotation = sender.translation(in: arView).y
            print("translation = \(newRotation)")
            direction = GLKMathRadiansToDegrees(virtualNode.eulerAngles.y)
            if newRotation < oldRotation {
                oldRotation = newRotation
                // putar kanan
                direction += 1.5
                virtualNode.eulerAngles.y = GLKMathDegreesToRadians(direction)
            } else if newRotation > oldRotation {
                oldRotation = newRotation
                // putar kiri
                direction -= 1.5
                virtualNode.eulerAngles.y = GLKMathDegreesToRadians(direction)
            }
        default:
            ()
        }

    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        print("camera did change \(camera.trackingState)")
        switch camera.trackingState {
        case .limited(_):
//            message = "tracking limited"
            measureButtonDisabled = true
        case .normal:
//            message =  "tracking ready"
            if isInsideBox == false {
                measureButtonDisabled = false
            }
        case .notAvailable:
//            message = "cannot track"
            measureButtonDisabled = true
        }
    }
            
    func renderer(_ renderer: SCNSceneRenderer, willRenderScene scene: SCNScene, atTime time: TimeInterval) {
        if isInsideBox == true { return }
        if measureButtonDisabled == true { return }
        if isFeetDrawn == false { return }
        DispatchQueue.main.async {
            self.redrawFeet()
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if measureButtonDisabled == false {

            // Cast ARAnchor as ARPlaneAnchor
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
            print("ARDelegate::renderer didAdd")
            
            // Create SCNGeometry from ARPlaneAnchor details
            planeWidth = CGFloat(planeAnchor.extent.x)
            planeHeight = CGFloat(planeAnchor.extent.z)
            let planeGeometry = SCNPlane(width: planeWidth, height: planeHeight)
            
            // Add material to geometry
            let material = SCNMaterial()
            material.diffuse.contents = UIColor.black.withAlphaComponent(0.7)
            planeGeometry.materials = [material]
            
            // Create a SCNNode from geometry
            planeNode = SCNNode(geometry: planeGeometry)
            planeNode.position = SCNVector3(planeAnchor.center.x, planeAnchor.center.y, planeAnchor.center.z)
            planeNode.eulerAngles.x = -.pi / 2
            // Add the newly created plane node as a child of the node created for the ARAnchor
//            node.addChildNode(planeNode)
            
//            planes.append(planeNode)
            //        print("ARDelegate::renderer didAdd planes.count = \((planes.count))")
            //
            //        for (index, plane) in planes.enumerated() {
            //            print("plane-\(index) = \(plane.position)")
            //        }

            DispatchQueue.main.async {
                self.drawFeet()
            }
        } else {
            for plane in planes {
                plane.removeFromParentNode()
            }
        }
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if measureButtonDisabled == false {

            // Cast ARAnchor as ARPlaneAnchor, get the child node of the anchor, and cast that node's geometry as an SCNPlane
            guard
                let planeAnchor = anchor as? ARPlaneAnchor,
                let planeNode = node.childNodes.first,
                let planeGeometry = planeNode.geometry as? SCNPlane
            else { return }

            // Update the dimensions of the plane geometry based on the plane anchor.
            planeWidth = CGFloat(planeAnchor.extent.x)
            planeHeight = CGFloat(planeAnchor.extent.z)
            planeGeometry.width = planeWidth
            planeGeometry.height = planeHeight
            
            // Update the position of the plane node based on the plane anchor.
            planeNode.position = SCNVector3(planeAnchor.center.x, planeAnchor.center.y, planeAnchor.center.z)
    //        print("ARDelegate::renderer didUpdate planeWidth = \(planeWidth), planeHeight = \(planeHeight)")
    //        print("ARDelegate::renderer didUpdate planeNode.position = \(planeNode.position)")
    //        print("ARDelegate::renderer didUpdate virtualNode position = \(virtualNode.position)")
        } else {
            for plane in planes {
                plane.removeFromParentNode()
            }
        }
    }
        
    func drawFeet() {
        guard let arView = arView else { return }
        print("ARDelegate::drawFeet()")
        undoButtonDisabled = false
        if let result = raycastResult(fromLocation: arView.center) {
            addVirtualDot(result: result)
            addRuler(to: virtualNode)
            addFoot(to: virtualNode)
            addBorder(to: virtualNode)
            addSlider(to: virtualNode)
            addText(text: sizeFoot, to: sliderNode)
            addCurtain(to: virtualNode)
            okButtonDisabled = true
            measureButtonDisabled = false
            isFeetDrawn = true
            message = "Place the ruler"
        }
        print("ARDelegate::putFeet isFeetDrawn = \(isFeetDrawn)")
    }
    
    func removeFeet() {
        print("ARDelegate::removeFeet()")
        virtualNode.removeFromParentNode()
        rulerNode.removeFromParentNode()
        footNode.removeFromParentNode()
        borderNode.removeFromParentNode()
        sliderNode.removeFromParentNode()
        textNode.removeFromParentNode()
        for curtains in curtainNode {
            curtains.removeFromParentNode()
        }
        curtainAdded = false
        undoButtonDisabled = true
        okButtonDisabled = true
        measureButtonDisabled = false
        isFeetDrawn = false
        isInsideBox = false
        message = "Aim to floor"
        guard let arView = arView else { return }
        arView.addGestureRecognizer(panGesture)
    }

    func redrawFeet() {
        guard let arView = arView else { return }
        if let result = raycastResult(fromLocation: arView.center) {    // sebaiknya gunakan dispatchqueue main async
            virtualNode.simdWorldTransform = result.worldTransform
        }
    }
    
    func putFeet() {
        measureButtonDisabled = true
        okButtonDisabled = false
        message = "Move inside the box"
    }
    
    func insideBox() {
        guard let arView = arView else { return }
        arView.removeGestureRecognizer(panGesture)
        sliderNode.isHidden = false
        textNode.isHidden = false
        for curtains in curtainNode {
            curtains.isHidden = true
        }

        isInsideBox = true
        message = "Adjust size"
    }

        
    func moveSlider() {
        sliderNode.position.z = 0.00 - (sizeFoot / 100.00)
        updateText()
    }
    
    func resetTracking() {
        guard let arView = arView else { return }
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        arView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])    //
        isPlaneAnchored = false
    }
    
    func speechToggle() {
        speechDisabled.toggle()
        speech.doSpeak(with: speechDisabled ? EnumDirection.voiceDisabled : EnumDirection.voiceEnabled)
    }

    // MARK: - Public
    
    public func coachingOverlayViewWillActivate(_ coachingOverlayView: ARCoachingOverlayView) {
        print("AR Coaching View Activate")
        isCoachingActive = true
    }
    
    public func coachingOverlayViewDidDeactivate(_ coachingOverlayView: ARCoachingOverlayView) {
        print("AR Coaching View Deactivate")
        isCoachingActive = false
    }
    
    // MARK: - Private

    private var arView: ARSCNView?
    private var trackedNode:SCNNode?
        
    private func addVirtualDot(result: ARRaycastResult) {
        guard let arView = arView else { return }
//        virtualNode = GeometryUtils.createDot(result: result)
//        virtualNode.pivot = SCNMatrix4MakeTranslation(0, 0.2, 0)
        arView.scene.rootNode.addChildNode(virtualNode)
    }
    
    private func addRuler(to: SCNNode) {
        rulerNode = GeometryUtils.createRuler()
        rulerNode.pivot = SCNMatrix4MakeTranslation(0, -0.18, 0)
        to.addChildNode(rulerNode)
    }
    
    private func addFoot(to: SCNNode) {
        footNode = GeometryUtils.createFoot()
        footNode.pivot = SCNMatrix4MakeTranslation(0, -0.2, 0)
        to.addChildNode(footNode)
    }
    
    private func addBorder(to: SCNNode) {
        borderNode = GeometryUtils.createBorder()
        to.addChildNode(borderNode)
    }
    
    private func addSlider(to: SCNNode) {
        sliderNode = GeometryUtils.createBorder()
        sliderNode.isHidden = true
        sliderNode.position.z = 0.00 - (sizeFoot / 100.00)
        to.addChildNode(sliderNode)
    }
        
    private func addText(text: Any, to: SCNNode) {
        textNode = GeometryUtils.createText(text: text)
        textNode.isHidden = true
        textNode.position.x -= 0.085
        textNode.position.z -= 0.02
        to.addChildNode(textNode)
    }
    
    private func updateText() {
        let textGeometry = SCNText(string: "\(sizeFoot) cm", extrusionDepth: 1.0)
        textGeometry.firstMaterial?.diffuse.contents = UIColor.white
        textGeometry.extrusionDepth = 0.0
        textNode.geometry = textGeometry
    }
    
    private func addCurtain(to: SCNNode) {
        if curtainAdded == true { return }
//        let colorPalette = [UIColor.red, UIColor.green, UIColor.blue, UIColor.black]
        let colorPalette = [UIColor](repeating: UIColor.black.withAlphaComponent(0.9), count: 4)
        let footWidth : Float = 0.28
        let footHeight : Float = 0.4
        let curtainWidth : Float = 1.5
        let curtainHeight : Float = 1.5
        
        curtainNode[0] = GeometryUtils.createCurtain(width: CGFloat(curtainWidth), height: ((CGFloat(curtainHeight) * 2) + CGFloat(footHeight)), color: colorPalette[0])
        curtainNode[1] = GeometryUtils.createCurtain(width: CGFloat(curtainWidth), height: ((CGFloat(curtainHeight) * 2) + CGFloat(footHeight)), color: colorPalette[1])
        curtainNode[2] = GeometryUtils.createCurtain(width: CGFloat(footWidth), height: CGFloat(curtainHeight), color: colorPalette[2])
        curtainNode[3] = GeometryUtils.createCurtain(width: CGFloat(footWidth), height: CGFloat(curtainHeight), color: colorPalette[3])
        
        curtainNode[0].pivot = SCNMatrix4MakeTranslation((footWidth / 2) + (curtainWidth / 2), 0.00 - (footHeight / 2), 0)
        curtainNode[1].pivot = SCNMatrix4MakeTranslation(0.00 - ((footWidth / 2) + (curtainWidth / 2)), 0.00 - (footHeight / 2), 0)
        curtainNode[2].pivot = SCNMatrix4MakeTranslation(0, curtainHeight / 2, 0)
        curtainNode[3].pivot = SCNMatrix4MakeTranslation(0, 0.00 - (footHeight + (curtainHeight / 2)), 0)
        for curtains in curtainNode {
            to.addChildNode(curtains)
        }
        curtainAdded = true
    }
    
    private func raycastResult(fromLocation location: CGPoint) -> ARRaycastResult? {
        guard let arView = arView,
              let query = arView.raycastQuery(from: location,
                                              allowing: .estimatedPlane,
                                              alignment: .horizontal) else { return nil }
        let results = arView.session.raycast(query)
        return results.first
    }
    
}

