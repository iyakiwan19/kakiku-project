//
//  TypeFootViewModel.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 29/06/22.
//

import Foundation

class TypeFootViewModel: ObservableObject {
    @Published var sizeFoot: Float = 0.0
    @Published var footTypeSelected: TypeFoot = TypeFoot.normal
    @Published var footTypeSelectedIndex = 0
    @Published var footTypeIsSelected: Bool = false
    @Published var showTypeFootSheet: Bool = false
    @Published var showRecommendationSheet: Bool = false
    @Published var showTypeFootDetailSheet: Bool = false
    
    @Published var dummyTypeFoot: [TypeFootModel] = [
        .init(id: 0, name: TypeFoot.normal, imageName: "FootNormal", description: "The foot lands on the outside of the heel and then rolls inward to absorb shock and support body weight.", isSelected: false),
        .init(id: 1, name: TypeFoot.underpronation, imageName: "FootUnderpronation", description: "The foot lands on the inside of the heel, then roll inward excessively, transferring weight to the inner edge instead of the ball of the foot.", isSelected: false),
        .init(id: 2, name: TypeFoot.overpronation, imageName: "FootOverpronation", description: "The outer side of the heel hits the ground, causing a large transmission of shock through the lower leg.", isSelected: false)
    ]
}
