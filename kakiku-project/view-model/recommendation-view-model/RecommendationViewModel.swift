//
//  RecommendationViewModel.swift
//  kakiku-project
//
//  Created by Taufiq Ichwanusofa on 27/06/22.
//

import Foundation

class RecommendationViewModel: ObservableObject {
    @Published var brandSelected = "Adidas"
    @Published var genderSelected = "Women"
    
    private var descriptionFootType = [
        "Universal": "This shoe is designed for all-around foot shape, good stability, and motion control, also enough cushioning",
        "Nike-Underpronation": "This shoes has perfect cushioning to absorbs the impact, and wide toe box. Having more space helps to prevent plantar fasciitis",
        "Nike-Normal": "This shoes offer neutral running, responsive, stability, energy returning, and keep you enjoy with every step",
        "Nike-Overpronation": "This shoes wll control the motion in your foot to varying degrees and help you to minimize your risk of injury",
        "Adidas-Underpronation": "This shoes is designed more cushioning in the midsore to adapt with the surface and shock absorption",
        "Adidas-Normal": "This shoes offer neutral running, responsive, energy returning cushioning, and keep you focused with every step",
        "Adidas-Overpronation": "This shoes designed for extra arch support and stability to prevent your foot from turning too far inward",
        "Asics-Underpronation": "This shoes is focused on midsole cushioning for extra shock absorption, and counter outward roll of foot",
        "Asics-Normal": "This shoes promote natural foot motion, and provide a feeling of more ground contact",
        "Asics-Overpronation": "This shoes help distribute the impact of running more effectively, support arch and medial post"
    ]
    
    func updateValue(type: String, value: String) {
        if type == "brand" {
            brandSelected = value
        } else {
            genderSelected = value
        }
    }
    
    func getDescriptionFootType(footType: String, brand: String) -> String {
        switch brand {
        case "Converse", "Vans":
            return self.descriptionFootType["Universal", default: "--"]
        default:
            return self.descriptionFootType["\(brand)-\(footType)", default: "--"]
        }
    }
    
    func getEUSize(brand: String, gender: String, footSize: Float) -> String {
        if brand == "Nike" {
            if gender == "Men" {
                switch footSize {
                case 22.5:
                    return "35.5"
                case 22.6..<23.1:
                    return "36"
                case 23.1..<23.6:
                    return "36.5"
                case 23.6..<23.76:
                    return "37.5"
                case 23.76..<24.1:
                    return "38"
                case 24.1..<24.25:
                    return "38.5"
                case 24.25..<24.6:
                    return "39"
                case 24.6..<25.1:
                    return "40"
                case 25.1..<25.6:
                    return "40.5"
                case 25.6..<26.1:
                    return "41"
                case 26.1..<26.6:
                    return "42"
                case 26.6..<27.1:
                    return "42.5"
                case 27.1..<27.6:
                    return "43"
                case 27.6..<28.1:
                    return "44"
                case 28.1..<28.6:
                    return "44.5"
                case 28.6..<29.1:
                    return "45"
                case 29.1..<29.6:
                    return "45.5"
                case 29.6..<30.1:
                    return "46"
                case 30.1..<30.6:
                    return "47"
                case 30.6..<31.1:
                    return "47.5"
                case 31.1..<31.6:
                    return "48"
                case 31.6..<32.1:
                    return "48.5"
                case 32.1..<32.6:
                    return "49"
                case 32.6..<33.1:
                    return "49.5"
                case 33.1..<33.6:
                    return "50"
                case 33.6..<34.1:
                    return "50.5"
                case 34.1..<34.6:
                    return "51"
                case 34.6..<35.1:
                    return "51.5"
                case 35.1..<35.6:
                    return "52"
                case 35.6..<36.1:
                    return "52.5"
                case 36.1..<36.6:
                    return "53"
                case 36.6..<37.1:
                    return "53.5"
                case 37.1..<37.6:
                    return "54"
                case 37.6..<38.1:
                    return "54.5"
                case 38.1..<38.6:
                    return "55"
                case 38.6..<39.1:
                    return "55.5"
                case 39.1..<39.6:
                    return "56"
                case 39.6..<40.1:
                    return "56.5"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 21.0:
                    return "34.5"
                case 21.1..<21.6:
                    return "35"
                case 21.6..<22.1:
                    return "35.5"
                case 22.1..<22.6:
                    return "36"
                case 22.6..<23.1:
                    return "36.5"
                case 23.1..<23.6:
                    return "37.5"
                case 23.6..<24.1:
                    return "38"
                case 24.1..<24.6:
                    return "38.5"
                case 24.6..<25.1:
                    return "39"
                case 25.1..<25.6:
                    return "40"
                case 25.6..<26.1:
                    return "40.5"
                case 26.1..<26.6:
                    return "41"
                case 26.6..<27.1:
                    return "42"
                case 27.1..<27.6:
                    return "42.5"
                case 27.6..<28.1:
                    return "43"
                case 28.1..<28.6:
                    return "44"
                case 28.6..<29.1:
                    return "44.5"
                case 29.1..<29.6:
                    return "45"
                case 29.6..<30.1:
                    return "45.5"
                case 30.1..<30.6:
                    return "46"
                case 30.6..<31.1:
                    return "47"
                case 31.1..<31.6:
                    return "47.5"
                case 31.6..<32.1:
                    return "48"
                case 32.1..<32.6:
                    return "48.5"
                case 32.6..<33.1:
                    return "49"
                case 33.1..<33.6:
                    return "50"
                case 33.6..<34.1:
                    return "50.5"
                case 34.1..<34.6:
                    return "51"
                case 34.6..<35.1:
                    return "51.5"
                case 35.1..<35.6:
                    return "52"
                case 35.6..<36.1:
                    return "52.5"
                case 36.1..<36.6:
                    return "53"
                case 36.6..<37.1:
                    return "53.5"
                case 37.1..<37.6:
                    return "54"
                case 37.6..<38.1:
                    return "54.5"
                case 38.1..<38.6:
                    return "55"
                case 38.6..<39.1:
                    return "55.5"
                case 39.1..<39.6:
                    return "56"
                default:
                    return "--"
                }
            }
        } else if brand == "Adidas" {
            switch footSize {
            case 22.1:
                return "36"
            case 22.2..<22.6:
                return "36.7"
            case 22.6..<23.0:
                return "37.3"
            case 23.0..<23.4:
                return "38"
            case 23.4..<23.9:
                return "38.7"
            case 23.9..<24.3:
                return "39.3"
            case 24.3..<24.7:
                return "40"
            case 24.7..<25.1:
                return "40.7"
            case 25.1..<25.6:
                return "41.7"
            case 25.6..<26.0:
                return "42"
            case 26.0..<26.4:
                return "42.7"
            case 26.4..<26.8:
                return "43.3"
            case 26.8..<27.2:
                return "44"
            case 27.2..<27.7:
                return "44.7"
            case 27.7..<28.1:
                return "45.3"
            case 28.1..<28.5:
                return "46"
            case 28.5..<28.9:
                return "46.7"
            case 28.9..<29.4:
                return "47.3"
            case 29.4..<29.8:
                return "48"
            case 29.8..<30.2:
                return "48.7"
            case 30.2..<30.6:
                return "49.3"
            case 30.6..<31.1:
                return "50"
            case 31.1..<31.5:
                return "50.7"
            default:
                return "--"
            }
        } else if brand == "Vans" {
            switch footSize {
            case 21.5:
                return "34.5"
            case 21.6..<22.1:
                return "35"
            case 22.1..<22.6:
                return "36"
            case 22.6..<23.1:
                return "36.5"
            case 23.1..<23.6:
                return "37"
            case 23.6..<24.1:
                return "38"
            case 24.1..<24.6:
                return "38.5"
            case 24.6..<25.1:
                return "39"
            case 25.1..<25.6:
                return "40"
            case 25.6..<26.1:
                return "40.5"
            case 26.1..<26.6:
                return "41"
            case 26.6..<27.1:
                return "42"
            case 27.1..<27.6:
                return "42.5"
            case 27.6..<28.1:
                return "43"
            case 28.1..<28.6:
                return "44"
            case 28.6..<29.1:
                return "44.5"
            case 29.1..<29.6:
                return "45"
            case 29.6..<30.1:
                return "46"
            case 30.1..<31.1:
                return "47"
            case 31.1..<32.1:
                return "48"
            case 32.1..<33.1:
                return "49"
            case 33.1..<34.1:
                return "50"
            default:
                return "--"
            }
        } else if brand == "Asics" {
            if gender == "Men" {
                switch footSize {
                case 22.50:
                    return "36"
                case 22.51..<23.01:
                    return "37"
                case 23.01..<23.51:
                    return "37.5"
                case 23.51..<24.01:
                    return "38"
                case 24.01..<24.51:
                    return "39"
                case 24.51..<25.01:
                    return "39.5"
                case 25.01..<25.26:
                    return "40"
                case 25.26..<25.51:
                    return "40.5"
                case 25.51..<26.01:
                    return "41.5"
                case 26.01..<26.51:
                    return "42"
                case 26.51..<27.01:
                    return "42.5"
                case 27.01..<27.51:
                    return "43.5"
                case 27.51..<28.01:
                    return "44"
                case 28.01..<28.26:
                    return "44.5"
                case 28.26..<28.51:
                    return "45"
                case 28.51..<29.01:
                    return "46"
                case 29.01..<29.51:
                    return "46.5"
                case 29.51..<30.01:
                    return "47"
                case 30.01..<30.51:
                    return "48"
                case 30.51..<31.01:
                    return "49"
                case 31.01..<32.01:
                    return "50.5"
                case 32.01..<33.01:
                    return "51.5"
                case 33.01..<34.01:
                    return "53"
                case 34.01..<34.51:
                    return "54"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 22.50:
                    return "35.5"
                case 22.51..<22.76:
                    return "36"
                case 22.76..<23.01:
                    return "37"
                case 23.01..<23.51:
                    return "37.5"
                case 23.51..<24.01:
                    return "38"
                case 24.01..<24.51:
                    return "39"
                case 24.51..<25.01:
                    return "39.5"
                case 25.01..<25.51:
                    return "40"
                case 25.51..<25.76:
                    return "40.5"
                case 25.76..<26.01:
                    return "41.5"
                case 26.01..<26.51:
                    return "42"
                case 26.51..<27.01:
                    return "42.5"
                case 27.01..<27.51:
                    return "43.5"
                case 27.51..<28.01:
                    return "44"
                case 28.01..<28.51:
                    return "44.5"
                case 28.51..<28.76:
                    return "45"
                case 28.76..<29.01:
                    return "46"
                case 29.01..<29.51:
                    return "46.5"
                case 29.51..<30.01:
                    return "47"
                default:
                    return "--"
                }
            }
        }
        else {
            return "--"
        }
    }
    
    func getUKSize(brand: String, gender: String, footSize: Float) -> String {
        if brand == "Nike" {
            if gender == "Men" {
                switch footSize {
                case 22.5:
                    return "3.0"
                case 22.6..<23.1:
                    return "3.5"
                case 23.1..<23.6:
                    return "4.0"
                case 23.5:
                    return "4.5"
                case 23.6..<24.1:
                    return "5.0"
                case 24.0:
                    return "5.5"
                case 24.1..<24.6:
                    return "6.0"
                case 24.6..<25.1:
                    return "6.0"
                case 25.1..<25.6:
                    return "6.5"
                case 25.6..<26.1:
                    return "7.0"
                case 26.1..<26.6:
                    return "7.5"
                case 26.6..<27.1:
                    return "8.0"
                case 27.1..<27.6:
                    return "8.5"
                case 27.6..<28.1:
                    return "9.0"
                case 28.1..<28.6:
                    return "9.5"
                case 28.6..<29.1:
                    return "10.0"
                case 29.1..<29.6:
                    return "10.5"
                case 29.6..<30.1:
                    return "11.0"
                case 30.1..<30.6:
                    return "11.5"
                case 30.6..<31.1:
                    return "12.0"
                case 31.1..<31.6:
                    return "12.5"
                case 31.6..<32.1:
                    return "13.0"
                case 32.1..<32.6:
                    return "13.5"
                case 32.6..<33.1:
                    return "14.0"
                case 33.1..<33.6:
                    return "14.5"
                case 33.6..<34.1:
                    return "15.0"
                case 34.1..<34.6:
                    return "15.5"
                case 34.6..<35.1:
                    return "16.0"
                case 35.1..<35.6:
                    return "16.5"
                case 35.6..<36.1:
                    return "17.0"
                case 36.1..<36.6:
                    return "17.5"
                case 36.6..<37.1:
                    return "18.0"
                case 37.1..<37.6:
                    return "18.5"
                case 37.6..<38.1:
                    return "19.0"
                case 38.1..<38.6:
                    return "19.5"
                case 38.6..<39.1:
                    return "20.0"
                case 39.1..<39.6:
                    return "20.5"
                case 39.6..<40.1:
                    return "21.0"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 21.0:
                    return "1.5"
                case 21.1..<21.6:
                    return "2.0"
                case 21.6..<22.1:
                    return "2.5"
                case 22.1..<22.6:
                    return "3.0"
                case 22.6..<23.1:
                    return "3.5"
                case 23.1..<23.6:
                    return "4.0"
                case 23.6..<24.1:
                    return "4.5"
                case 24.1..<24.6:
                    return "5.0"
                case 24.6..<25.1:
                    return "5.5"
                case 25.1..<25.6:
                    return "6.0"
                case 25.6..<26.1:
                    return "6.5"
                case 26.1..<26.6:
                    return "7.0"
                case 26.6..<27.1:
                    return "7.5"
                case 27.1..<27.6:
                    return "8.0"
                case 27.6..<28.1:
                    return "8.5"
                case 28.1..<28.6:
                    return "9.0"
                case 28.6..<29.1:
                    return "9.5"
                case 29.1..<29.6:
                    return "10.0"
                case 29.6..<30.1:
                    return "10.5"
                case 30.1..<30.6:
                    return "11.0"
                case 30.6..<31.1:
                    return "11.5"
                case 31.1..<31.6:
                    return "12.0"
                case 31.6..<32.1:
                    return "12.5"
                case 32.1..<32.6:
                    return "13.0"
                case 32.6..<33.1:
                    return "13.5"
                case 33.1..<33.6:
                    return "14.0"
                case 33.6..<34.1:
                    return "14.5"
                case 34.1..<34.6:
                    return "15.0"
                case 34.6..<35.1:
                    return "15.5"
                case 35.1..<35.6:
                    return "16.0"
                case 35.6..<36.1:
                    return "16.5"
                case 36.1..<36.6:
                    return "17.0"
                case 36.6..<37.1:
                    return "17.5"
                case 37.1..<37.6:
                    return "18.0"
                case 37.6..<38.1:
                    return "18.5"
                case 38.1..<38.6:
                    return "19.0"
                case 38.6..<39.1:
                    return "19.5"
                case 39.1..<39.6:
                    return "20.0"
                default:
                    return "--"
                }
            }
        } else if brand == "Adidas" {
            switch footSize {
            case 22.1:
                return "3.5"
            case 22.2..<22.6:
                return "4.0"
            case 22.6..<23.0:
                return "4.5"
            case 23.0..<23.4:
                return "5.0"
            case 23.4..<23.9:
                return "5.5"
            case 23.9..<24.3:
                return "6.0"
            case 24.3..<24.7:
                return "6.5"
            case 24.7..<25.1:
                return "7.0"
            case 25.1..<25.6:
                return "7.5"
            case 25.6..<26.0:
                return "8.0"
            case 26.0..<26.4:
                return "8.5"
            case 26.4..<26.8:
                return "9.0"
            case 26.8..<27.2:
                return "9.5"
            case 27.2..<27.7:
                return "10.0"
            case 27.7..<28.1:
                return "10.5"
            case 28.1..<28.5:
                return "11.0"
            case 28.5..<28.9:
                return "11.5"
            case 28.9..<29.4:
                return "12.0"
            case 29.4..<29.8:
                return "12.5"
            case 29.8..<30.2:
                return "13.0"
            case 30.2..<30.6:
                return "13.5"
            case 30.6..<31.1:
                return "14.0"
            case 31.1..<31.5:
                return "14.5"
            default:
                return "--"
            }
        } else if brand == "Vans" {
            switch footSize {
            case 21.5:
                return "2.5"
            case 21.6..<22.1:
                return "3.0"
            case 22.1..<22.6:
                return "3.5"
            case 22.6..<23.1:
                return "4.0"
            case 23.1..<23.6:
                return "4.5"
            case 23.6..<24.1:
                return "5.0"
            case 24.1..<24.6:
                return "5.5"
            case 24.6..<25.1:
                return "6.0"
            case 25.1..<25.6:
                return "6.5"
            case 25.6..<26.1:
                return "7.0"
            case 26.1..<26.6:
                return "7.5"
            case 26.6..<27.1:
                return "8.0"
            case 27.1..<27.6:
                return "8.5"
            case 27.6..<28.1:
                return "9.0"
            case 28.1..<28.6:
                return "9.5"
            case 28.6..<29.1:
                return "10.0"
            case 29.1..<29.6:
                return "10.5"
            case 29.6..<30.1:
                return "11.0"
            case 30.1..<31.1:
                return "12.0"
            case 31.1..<32.1:
                return "13.0"
            case 32.1..<33.1:
                return "14.0"
            case 33.1..<34.1:
                return "15.0"
            default:
                return "--"
            }
        } else if brand == "Asics" {
            if gender == "Men" {
                switch footSize {
                case 22.50:
                    return "3.0"
                case 22.51..<23.01:
                    return "3.5"
                case 23.01..<23.51:
                    return "4.0"
                case 23.51..<24.01:
                    return "4.5"
                case 24.01..<24.51:
                    return "5.0"
                case 24.51..<25.01:
                    return "5.5"
                case 25.01..<25.26:
                    return "6.0"
                case 25.26..<25.51:
                    return "6.5"
                case 25.51..<26.01:
                    return "7.0"
                case 26.01..<26.51:
                    return "7.5"
                case 26.51..<27.01:
                    return "8.0"
                case 27.01..<27.51:
                    return "8.5"
                case 27.51..<28.01:
                    return "9.0"
                case 28.01..<28.26:
                    return "9.5"
                case 28.26..<28.51:
                    return "10.0"
                case 28.51..<29.01:
                    return "10.5"
                case 29.01..<29.51:
                    return "11.0"
                case 29.51..<30.01:
                    return "11.5"
                case 30.01..<30.51:
                    return "12.0"
                case 30.51..<31.01:
                    return "13.0"
                case 31.01..<32.01:
                    return "14.0"
                case 32.01..<33.01:
                    return "15.0"
                case 33.01..<34.01:
                    return "16.0"
                case 34.01..<34.51:
                    return "17.0"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 22.50:
                    return "3.0"
                case 22.51..<22.76:
                    return "3.5"
                case 22.76..<23.01:
                    return "4.0"
                case 23.01..<23.51:
                    return "4.5"
                case 23.51..<24.01:
                    return "5.0"
                case 24.01..<24.51:
                    return "5.5"
                case 24.51..<25.01:
                    return "6.0"
                case 25.01..<25.51:
                    return "6.5"
                case 25.51..<25.76:
                    return "7.0"
                case 25.76..<26.01:
                    return "7.5"
                case 26.01..<26.51:
                    return "8.0"
                case 26.51..<27.01:
                    return "8.5"
                case 27.01..<27.51:
                    return "9.0"
                case 27.51..<28.01:
                    return "9.5"
                case 28.01..<28.51:
                    return "10.0"
                case 28.51..<28.76:
                    return "10.5"
                case 28.76..<29.01:
                    return "11.0"
                case 29.01..<29.51:
                    return "11.5"
                case 29.51..<30.01:
                    return "12.0"
                default:
                    return "--"
                }
            }
        }
        else {
            return "--"
        }
    }
    
    func getUSSize(brand: String, gender: String, footSize: Float) -> String {
        if brand == "Nike" {
            if gender == "Men" {
                switch footSize {
                case 22.5:
                    return "3.5"
                case 22.6..<23.1:
                    return "4.0"
                case 23.1..<23.6:
                    return "4.5"
                case 23.5:
                    return "5.0"
                case 23.6..<24.1:
                    return "5.5"
                case 24.0:
                    return "6.0"
                case 24.1..<24.6:
                    return "6.5"
                case 24.6..<25.1:
                    return "7.0"
                case 25.1..<25.6:
                    return "7.5"
                case 25.6..<26.1:
                    return "8.0"
                case 26.1..<26.6:
                    return "8.5"
                case 26.6..<27.1:
                    return "9.0"
                case 27.1..<27.6:
                    return "9.5"
                case 27.6..<28.1:
                    return "10.0"
                case 28.1..<28.6:
                    return "10.5"
                case 28.6..<29.1:
                    return "11.0"
                case 29.1..<29.6:
                    return "11.5"
                case 29.6..<30.1:
                    return "12.0"
                case 30.1..<30.6:
                    return "12.5"
                case 30.6..<31.1:
                    return "13.0"
                case 31.1..<31.6:
                    return "13.5"
                case 31.6..<32.1:
                    return "14.0"
                case 32.1..<32.6:
                    return "14.5"
                case 32.6..<33.1:
                    return "15.0"
                case 33.1..<33.6:
                    return "15.5"
                case 33.6..<34.1:
                    return "16.0"
                case 34.1..<34.6:
                    return "16.5"
                case 34.6..<35.1:
                    return "17.0"
                case 35.1..<35.6:
                    return "17.5"
                case 35.6..<36.1:
                    return "18.0"
                case 36.1..<36.6:
                    return "18.5"
                case 36.6..<37.1:
                    return "19.0"
                case 37.1..<37.6:
                    return "19.5"
                case 37.6..<38.1:
                    return "20.0"
                case 38.1..<38.6:
                    return "20.5"
                case 38.6..<39.1:
                    return "21.0"
                case 39.1..<39.6:
                    return "21.5"
                case 39.6..<40.1:
                    return "22.0"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 21.0:
                    return "4.0"
                case 21.1..<21.6:
                    return "4.5"
                case 21.6..<22.1:
                    return "5.0"
                case 22.1..<22.6:
                    return "5.5"
                case 22.6..<23.1:
                    return "6.0"
                case 23.1..<23.6:
                    return "6.5"
                case 23.6..<24.1:
                    return "7.0"
                case 24.1..<24.6:
                    return "7.5"
                case 24.6..<25.1:
                    return "8.0"
                case 25.1..<25.6:
                    return "8.5"
                case 25.6..<26.1:
                    return "9.0"
                case 26.1..<26.6:
                    return "9.5"
                case 26.6..<27.1:
                    return "10.0"
                case 27.1..<27.6:
                    return "10.5"
                case 27.6..<28.1:
                    return "11.0"
                case 28.1..<28.6:
                    return "11.5"
                case 28.6..<29.1:
                    return "12.0"
                case 29.1..<29.6:
                    return "12.5"
                case 29.6..<30.1:
                    return "13.0"
                case 30.1..<30.6:
                    return "13.5"
                case 30.6..<31.1:
                    return "14.0"
                case 31.1..<31.6:
                    return "14.5"
                case 31.6..<32.1:
                    return "15.0"
                case 32.1..<32.6:
                    return "15.5"
                case 32.6..<33.1:
                    return "16.0"
                case 33.1..<33.6:
                    return "16.5"
                case 33.6..<34.1:
                    return "17.0"
                case 34.1..<34.6:
                    return "17.5"
                case 34.6..<35.1:
                    return "18.0"
                case 35.1..<35.6:
                    return "18.5"
                case 35.6..<36.1:
                    return "19.0"
                case 36.1..<36.6:
                    return "19.5"
                case 36.6..<37.1:
                    return "20.0"
                case 37.1..<37.6:
                    return "20.5"
                case 37.6..<38.1:
                    return "21.0"
                case 38.1..<38.6:
                    return "21.5"
                case 38.6..<39.1:
                    return "22.0"
                case 39.1..<39.6:
                    return "22.5"
                default:
                    return "--"
                }
            }
        } else if brand == "Adidas" {
            if gender == "Men" {
                switch footSize {
                case 22.1:
                    return "4.0"
                case 22.2..<22.6:
                    return "4.5"
                case 22.6..<23.0:
                    return "5.0"
                case 23.0..<23.4:
                    return "5.5"
                case 23.4..<23.9:
                    return "6.0"
                case 23.9..<24.3:
                    return "6.5"
                case 24.3..<24.7:
                    return "7.0"
                case 24.7..<25.1:
                    return "7.5"
                case 25.1..<25.6:
                    return "8.0"
                case 25.6..<26.0:
                    return "8.5"
                case 26.0..<26.4:
                    return "9.0"
                case 26.4..<26.8:
                    return "9.5"
                case 26.8..<27.2:
                    return "10.0"
                case 27.2..<27.7:
                    return "10.5"
                case 27.7..<28.1:
                    return "11.0"
                case 28.1..<28.5:
                    return "11.5"
                case 28.5..<28.9:
                    return "12.0"
                case 28.9..<29.4:
                    return "12.5"
                case 29.4..<29.8:
                    return "13.0"
                case 29.8..<30.2:
                    return "13.5"
                case 30.2..<30.6:
                    return "14.0"
                case 30.6..<31.1:
                    return "14.5"
                case 31.1..<31.5:
                    return "15.0"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 22.1:
                    return "5.0"
                case 22.2..<22.6:
                    return "5.5"
                case 22.6..<23.0:
                    return "6.0"
                case 23.0..<23.4:
                    return "6.5"
                case 23.4..<23.9:
                    return "7.0"
                case 23.9..<24.3:
                    return "7.5"
                case 24.3..<24.7:
                    return "8.0"
                case 24.7..<25.1:
                    return "8.5"
                case 25.1..<25.6:
                    return "9.0"
                case 25.6..<26.0:
                    return "9.5"
                case 26.0..<26.4:
                    return "10.0"
                case 26.4..<26.8:
                    return "10.5"
                case 26.8..<27.2:
                    return "11.0"
                case 27.2..<27.7:
                    return "11.5"
                case 27.7..<28.1:
                    return "12.0"
                case 28.1..<28.5:
                    return "12.5"
                case 28.5..<28.9:
                    return "13.0"
                case 28.9..<29.4:
                    return "13.5"
                case 29.4..<29.8:
                    return "14.0"
                case 29.8..<30.2:
                    return "14.5"
                case 30.2..<30.6:
                    return "15.0"
                case 30.6..<31.1:
                    return "15.5"
                default:
                    return "--"
                }
            }
        } else if brand == "Vans" {
            if gender == "Men" {
                switch footSize {
                case 21.5:
                    return "3.5"
                case 21.6..<22.1:
                    return "4.0"
                case 22.1..<22.6:
                    return "4.5"
                case 22.6..<23.1:
                    return "5.0"
                case 23.1..<23.6:
                    return "5.5"
                case 23.6..<24.1:
                    return "6.0"
                case 24.1..<24.6:
                    return "6.5"
                case 24.6..<25.1:
                    return "7.0"
                case 25.1..<25.6:
                    return "7.5"
                case 25.6..<26.1:
                    return "8.0"
                case 26.1..<26.6:
                    return "8.5"
                case 26.6..<27.1:
                    return "9.0"
                case 27.1..<27.6:
                    return "9.5"
                case 27.6..<28.1:
                    return "10.0"
                case 28.1..<28.6:
                    return "10.5"
                case 28.6..<29.1:
                    return "11.0"
                case 29.1..<29.6:
                    return "11.5"
                case 29.6..<30.1:
                    return "12.0"
                case 30.1..<31.1:
                    return "13.0"
                case 31.1..<32.1:
                    return "14.0"
                case 32.1..<33.1:
                    return "15.0"
                case 33.1..<34.1:
                    return "16.0"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 21.5:
                    return "5.0"
                case 21.6..<22.1:
                    return "5.5"
                case 22.1..<22.6:
                    return "6.0"
                case 22.6..<23.1:
                    return "6.5"
                case 23.1..<23.6:
                    return "7.0"
                case 23.6..<24.1:
                    return "7.5"
                case 24.1..<24.6:
                    return "8.0"
                case 24.6..<25.1:
                    return "8.5"
                case 25.1..<25.6:
                    return "9.0"
                case 25.6..<26.1:
                    return "9.5"
                case 26.1..<26.6:
                    return "10.0"
                case 26.6..<27.1:
                    return "10.5"
                case 27.1..<27.6:
                    return "11.0"
                case 27.6..<28.1:
                    return "11.5"
                case 28.1..<28.6:
                    return "12.0"
                case 28.6..<29.1:
                    return "12.5"
                case 29.1..<29.6:
                    return "13.0"
                case 29.6..<30.1:
                    return "13.5"
                case 30.1..<31.1:
                    return "14.0"
                case 31.1..<32.1:
                    return "14.5"
                case 32.1..<33.1:
                    return "15.0"
                case 33.1..<34.1:
                    return "15.5"
                default:
                    return "--"
                }
            }
        } else if brand == "Asics" {
            if gender == "Men" {
                switch footSize {
                case 22.50:
                    return "4.0"
                case 22.51..<23.01:
                    return "4.5"
                case 23.01..<23.51:
                    return "5.0"
                case 23.51..<24.01:
                    return "5.5"
                case 24.01..<24.51:
                    return "6.0"
                case 24.51..<25.01:
                    return "6.5"
                case 25.01..<25.26:
                    return "7.0"
                case 25.26..<25.51:
                    return "7.5"
                case 25.51..<26.01:
                    return "8.0"
                case 26.01..<26.51:
                    return "8.5"
                case 26.51..<27.01:
                    return "9.0"
                case 27.01..<27.51:
                    return "9.5"
                case 27.51..<28.01:
                    return "10.0"
                case 28.01..<28.26:
                    return "10.5"
                case 28.26..<28.51:
                    return "11.0"
                case 28.51..<29.01:
                    return "11.5"
                case 29.01..<29.51:
                    return "12.0"
                case 29.51..<30.01:
                    return "12.5"
                case 30.01..<30.51:
                    return "13.0"
                case 30.51..<31.01:
                    return "14.0"
                case 31.01..<32.01:
                    return "15.0"
                case 32.01..<33.01:
                    return "16.0"
                case 33.01..<34.01:
                    return "17.0"
                case 34.01..<34.51:
                    return "18.0"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 22.50:
                    return "5.0"
                case 22.51..<22.76:
                    return "5.5"
                case 22.76..<23.01:
                    return "6.0"
                case 23.01..<23.51:
                    return "6.5"
                case 23.51..<24.01:
                    return "7.0"
                case 24.01..<24.51:
                    return "7.5"
                case 24.51..<25.01:
                    return "8.0"
                case 25.01..<25.51:
                    return "8.5"
                case 25.51..<25.76:
                    return "9.0"
                case 25.76..<26.01:
                    return "9.5"
                case 26.01..<26.51:
                    return "10.0"
                case 26.51..<27.01:
                    return "10.5"
                case 27.01..<27.51:
                    return "11.0"
                case 27.51..<28.01:
                    return "11.5"
                case 28.01..<28.51:
                    return "12.0"
                case 28.51..<28.76:
                    return "12.5"
                case 28.76..<29.01:
                    return "13.0"
                case 29.01..<29.51:
                    return "13.5"
                case 29.51..<30.01:
                    return "14.0"
                default:
                    return "--"
                }
            }
        } else if brand == "Converse" {
            if gender == "Men" {
                switch footSize {
                case 21.50:
                    return "3.5"
                case 21.51..<21.91:
                    return "4.0"
                case 21.91..<22.41:
                    return "4.5"
                case 22.41..<22.81:
                    return "5.0"
                case 22.81..<23.21:
                    return "5.5"
                case 23.21..<23.61:
                    return "6.0"
                case 23.61..<24.11:
                    return "6.5"
                case 24.11..<24.51:
                    return "7.0"
                case 24.51..<24.91:
                    return "7.5"
                case 24.91..<25.31:
                    return "8.0"
                case 25.31..<25.81:
                    return "8.5"
                case 25.81..<26.21:
                    return "9.0"
                case 26.21..<26.41:
                    return "9.5"
                case 26.41..<26.61:
                    return "10.0"
                case 26.61..<27.41:
                    return "10.5"
                case 27.41..<27.91:
                    return "11.0"
                case 27.91..<28.31:
                    return "11.5"
                case 28.31..<28.71:
                    return "12.0"
                case 28.71..<28.91:
                    return "12.5"
                case 28.91..<29.11:
                    return "13.0"
                case 29.11..<29.61:
                    return "13.5"
                case 29.61..<30.41:
                    return "14.0"
                default:
                    return "--"
                }
            } else {
                switch footSize {
                case 21.10:
                    return "4.5"
                case 21.11..<21.91:
                    return "5.0"
                case 21.91..<22.41:
                    return "5.5"
                case 22.41..<22.81:
                    return "6.0"
                case 22.81..<23.21:
                    return "6.5"
                case 23.21..<23.61:
                    return "7.0"
                case 23.61..<24.11:
                    return "7.5"
                case 24.11..<24.51:
                    return "8.0"
                case 24.51..<24.91:
                    return "8.5"
                case 24.91..<25.31:
                    return "9.0"
                case 25.31..<25.81:
                    return "9.5"
                case 25.81..<26.21:
                    return "10.0"
                case 26.21..<26.61:
                    return "10.5"
                case 26.61..<27.41:
                    return "11.0"
                case 27.41..<27.91:
                    return "11.5"
                case 27.91..<28.31:
                    return "12.0"
                case 28.31..<28.71:
                    return "12.5"
                case 28.71..<28.91:
                    return "13.0"
                case 28.91..<29.11:
                    return "13.5"
                case 29.11..<29.61:
                    return "14.0"
                default:
                    return "--"
                }
            }
        }
        else {
            return "--"
        }
    }
    
    func getShoesImage(brand: String, footType: String) -> String {
        if brand == "Converse" {
            return "converse"
        } else if brand == "Vans" {
            return "vans"
        } else {
            switch (brand, footType) {
                case ("Adidas", "Normal"):
                    return "adidas_normal"
                case ("Adidas", "Underpronation"):
                    return "adidas_underpronation"
                case ("Adidas", "Overpronation"):
                    return "adidas_overpronation"
                case ("Nike", "Normal"):
                    return "nike_normal"
                case ("Nike", "Underpronation"):
                    return "nike_underpronation"
                case ("Nike", "Overpronation"):
                    return "nike_overpronation"
                case ("Asics", "Normal"):
                    return "asics_normal"
                case ("Asics", "Underpronation"):
                    return "asics_underpronation"
                default:
                    return "asics_overpronation"
            }
        }
    }
}
