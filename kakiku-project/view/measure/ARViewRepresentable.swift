//
//  ARViewRepresentable.swift
//  kakiku-project
//
//  Created by Gualtiero Frigerio on 18/05/21.
//  Edited by Ricky Suprayudi on 27/06/22.
//

import ARKit
import SwiftUI

struct ARViewRepresentable: UIViewRepresentable {   // view / measure
    let arDelegate:ARDelegate
    func makeUIView(context: Context) -> some UIView {
        let arView = ARSCNView(frame: .zero)
        arDelegate.setARView(arView)
        return arView
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}

struct ARViewRepresentable_Previews: PreviewProvider {
    static var previews: some View {
        ARViewRepresentable(arDelegate: ARDelegate())
    }
}
