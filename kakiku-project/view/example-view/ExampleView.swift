//
//  ExampleView.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 24/06/22.
//

import SwiftUI

struct ExampleView: View {
    @State private var textInput = ""
    let speechAction = Speech()
    @State private var showingSheet = false
    
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
        VStack(spacing: 20) {
            Button("Move closer") {
                speechAction.doSpeak(with: EnumDirection.closer)
            }
            Button("Move away") {
                speechAction.doSpeak(with: EnumDirection.away)
            }
            Button("Show Sheet") {
                showingSheet.toggle()
            }
        }.padding()
    }
}

struct ExampleView_Previews: PreviewProvider {
    static var previews: some View {
        ExampleView()
    }
}
