//
//  ContentView.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 20/06/22.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var arDelegate = ARDelegate()
    @State private var showingSheet = false
    @ObservedObject var viewModelMeasurement = MeasurementViewModel()
    
    var body: some View {
        ZStack {
            DismissGuardian(preventDismissal: $viewModelMeasurement.preventDismissal, attempted: $viewModelMeasurement.attempted) {
                ZStack {
                    ARViewRepresentable(arDelegate: arDelegate)
                    if arDelegate.isCoachingActive == false {
                        VStack {
                            HStack {
                                Text(arDelegate.message)
                                    .foregroundColor(Color.white)
                                    .font(.system(size: 18).bold())
                                    .frame(maxWidth: .infinity)
                            }
                            .padding(.top, 150)
                            Spacer()
                        }
                        VStack {
                            HStack {
                                Button {
                                    arDelegate.sizeFoot = 28.0
                                    arDelegate.removeFeet()
                                    arDelegate.resetTracking()
                                    print("remove ruler")
                                } label: {
                                    Label("remove ruler"    , systemImage: "arrow.uturn.backward.circle.fill")
                                        .labelStyle(IconOnlyLabelStyle())
                                        .font(.system(size: 35).bold())
                                        .foregroundColor(Color(arDelegate.undoButtonDisabled ? "UndoAndMarkButtonDisabled" : "UndoAndMarkButton"))
                                        .frame(width: 100, height: 100)
                                }
                                .disabled(arDelegate.undoButtonDisabled)
                                Spacer()
                                Button {
                                    if arDelegate.isInsideBox {
                                        print("Measure OK")
                                        arDelegate.pauseAR()
                                        showingSheet.toggle()
                                    } else {
                                        arDelegate.insideBox()
                                    }
                                } label: {
                                    Label("Measurement Done", systemImage: "checkmark.circle.fill")
                                        .labelStyle(IconOnlyLabelStyle())
                                        .font(.system(size: 35).bold())
                                        .foregroundColor(Color(arDelegate.okButtonDisabled ? "UndoAndMarkButtonDisabled" : "UndoAndMarkButton"))
                                        .frame(width: 100, height: 100)
                                }
                                .disabled(arDelegate.okButtonDisabled)
                                .sheet(isPresented: $showingSheet) {
                                    arDelegate.resumeAR()
                                } content: {
                                    ShapeFootView(sizeFoot:  arDelegate.sizeFoot, arDelegate: arDelegate).environmentObject(viewModelMeasurement)
                                }
                            }
                            .padding(.top, 50)
                            Spacer()
                            ZStack {
                                if arDelegate.isFeetDrawn {
                                    Button {
                                        arDelegate.putFeet()
                                        print("put ruler")
                                    } label: {
                                        ZStack {
                                            Text("")
                                                .frame(width: 81, height: 81)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 41)
                                                        .stroke(Color(arDelegate.measureButtonDisabled ? "UndoAndMarkButtonDisabled" : "UndoAndMarkButton"), lineWidth: 3))
                                            Text("")
                                                .frame(width: 64, height: 64)
                                                .background(arDelegate.measureButtonDisabled ? Color("UndoAndMarkButtonDisabled") : .white)
                                                .cornerRadius(32)
                                        }
                                    }
                                    .disabled(arDelegate.measureButtonDisabled)
                                    .padding(.bottom, 50)
                                }
                            }
                        }
                        if arDelegate.isInsideBox {
                            VStack {
                                Spacer()
                                Slider(value: Binding(get: {
                                    arDelegate.sizeFoot
                                }, set: { (newVal) in
                                    arDelegate.sizeFoot = newVal
                                    arDelegate.moveSlider()
                                }), in: 20.0...36.0, step: 0.5) {
                                    Text("Length")
                                }
                                .tint(Color("UndoAndMarkButton"))
                                .transformEffect(CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2)))
                            }
                            .padding(.trailing, -220)
                            .padding(.leading, 325)
                            .padding(.bottom, 250)
                        }
                    }
                }
                .ignoresSafeArea()
                .onAppear {
                    UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
                    AppDelegate.orientationLock = .portrait
                }
            }
        }
        .ignoresSafeArea()
        .onAppear {
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            AppDelegate.orientationLock = .portrait
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
