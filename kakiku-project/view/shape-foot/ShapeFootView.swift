//
//  ShapeFootView.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 01/07/22.
//

import SwiftUI

struct ShapeFootView: View {
    let sizeFoot: Float
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    @State var shapeFoot: ShapeFoot = ShapeFoot.step1
    @StateObject var viewModelRecommendation = RecommendationViewModel()
    @StateObject var viewModelTypeFoot = TypeFootViewModel()
    
    @EnvironmentObject var viewModelMeasurement: MeasurementViewModel
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var arDelegate: ARDelegate
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.black
                    .ignoresSafeArea()
                
                VStack(alignment: .center, spacing: 20) {
                    Spacer()
                    TabView(selection: $shapeFoot) {
                        ForEach(dummyShapeFoot) { shapeFoot in
                            ShapeFootItemView(shapeFoot: shapeFoot).tag(shapeFoot.name)
                        }
                    }
                    .frame(width: UIScreen.main.bounds.width, height: 300)
                    .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                    .onReceive(timer) { _ in
                        switch shapeFoot {
                        case .step1:
                            shapeFoot = ShapeFoot.step2
                        case .step2:
                            shapeFoot = ShapeFoot.step3
                        case .step3:
                            shapeFoot = ShapeFoot.step1
                        }
                    }
                    Spacer()
                    ListStepShapeFoot(shapeFoot: shapeFoot)
                    Spacer()
                    NavigationLink(destination: TypeFootView( viewModelRecommendation: viewModelRecommendation, viewModelTypeFoot: viewModelTypeFoot), isActive: $viewModelTypeFoot.showTypeFootSheet) {
                        Button(action: {
                            viewModelTypeFoot.sizeFoot = sizeFoot
                            viewModelTypeFoot.showTypeFootSheet = true
                        }) {
                            Text("Choose Foot Shape")
                                .frame(minWidth: 0, maxWidth: 250)
                                .font(.system(size: 18))
                                .padding()
                                .foregroundColor(.black)
                                .background(
                                    RoundedRectangle(cornerRadius: 10, style: .continuous).fill(Color("ColorButton"))
                                )
                        }
                    }
                }.padding()
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    VStack {
                        Spacer()
                        Image("PopHide").padding(.top, 50)

                        HStack {
                            Text("Find foot shape")
                                .font(.system(size: 32, weight: .heavy))
                                .foregroundColor(Color.white)
                                .multilineTextAlignment(.center)
                                .lineLimit(0)
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.top, 30)
                                .padding(.leading, 20)
                        }
                    }
                }
            }
        }
        .navigationBarHidden(true)
        .confirmationDialog("", isPresented: $viewModelMeasurement.attempted, titleVisibility: .automatic) {
            Button("Recapture", role: .destructive) {
                arDelegate.removeFeet()
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

struct ShapeFootView_Previews: PreviewProvider {
    static var previews: some View {
        ShapeFootView(sizeFoot: 10.0, arDelegate: ARDelegate()).environmentObject(MeasurementViewModel())
            .previewDevice("iPhone 13")
    }
}

struct ListStepShapeFoot: View {
    var shapeFoot: ShapeFoot
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            Divider().background(Color.white)
            Text("1. Put your foot into a water surface")
                .foregroundColor(shapeFoot == ShapeFoot.step1 ? Color("ColorButton") : .white)
                .padding(.leading, 20)
                .font(Font.headline.weight(shapeFoot == ShapeFoot.step1 ? .bold: .regular))
            Divider().background(Color.white)
            Text("2. Step your foot on a paper")
                .foregroundColor(shapeFoot == ShapeFoot.step2 ? Color("ColorButton") : .white)
                .padding(.leading, 20)
                .font(Font.headline.weight(shapeFoot == ShapeFoot.step2 ? .bold: .regular))
            Divider().background(Color.white)
            Text("3. You have a foot shape!")
                .foregroundColor(shapeFoot == ShapeFoot.step3 ? Color("ColorButton") : .white)
                .padding(.leading, 20)
                .font(Font.headline.weight(shapeFoot == ShapeFoot.step3 ? .bold: .regular))
            Divider().background(Color.white)
        }
    }
}
