//
//  ShapeFootItemView.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 14/07/22.
//

import SwiftUI

struct ShapeFootItemView: View {
    let shapeFoot: ShapeFootModel
    var body: some View {
        Image(shapeFoot.imageName)
            .resizable()
            .frame(width: 250, height: 250, alignment: .center)
            .padding(.top, 10)
    }
}

struct ShapeFootItemView_Previews: PreviewProvider {
    static var previews: some View {
        ShapeFootItemView(shapeFoot: dummyShapeFoot[0])
    }
}
