//
//  ShoesRecommendation.swift
//  kakiku-project
//
//  Created by Taufiq Ichwanusofa on 26/06/22.
//

import SwiftUI

struct ShoesRecommendation: View {
    @ObservedObject var viewModelRecommendation: RecommendationViewModel
    @ObservedObject var viewModelTypeFoot: TypeFootViewModel
    @State private var showTypeFootView = false
    
    let footSize: Float
    let footType: TypeFoot
    
    var shoesBrands = ["Adidas", "Nike", "Converse", "Vans", "Asics"]
    var genders = ["Men", "Women"]
    
    let shoesRecommendationModel = ShoesRecommendationModel()
    
    @State private var showBrandPicker = false
    @State private var showGenderPicker = false
    @State private var showAlertDismiss = false
    
    init(viewModelTypeFoot: TypeFootViewModel, viewModelRecommendation: RecommendationViewModel, footSize: Float, footType: TypeFoot) {
        let dataFootSize = Int((footSize * 10).rounded(.up))
        self.footSize = Float(Double(dataFootSize) / 10)
        self.footType = footType
        self.viewModelRecommendation = viewModelRecommendation
        self.viewModelTypeFoot = viewModelTypeFoot
    }
    
    var body: some View {
        ZStack {
            BlurView(style: .light)
                .opacity(0.85)
            
            VStack(alignment: .leading) {
                HStack {
                    Text(String(format: "%.1fcm", footSize))
                        .foregroundColor(Color.white)
                        .font(.system(size: 30))
                        .fontWeight(.heavy)
                    
                    NavigationLink(destination: TypeFootDetailView( viewModelRecommendation: viewModelRecommendation, viewModelTypeFoot: viewModelTypeFoot), isActive: $viewModelTypeFoot.showTypeFootDetailSheet){
                        Button("\(footType.rawValue)") {
                            viewModelTypeFoot.showTypeFootDetailSheet = true
                        }
                        .padding([.horizontal], 10)
                        .padding([.vertical], 5)
                        .foregroundColor(Color.white)
                        .background(Color.gray)
                        .cornerRadius(10)
                    }
                }
                .frame(alignment: .leading)
                
                Text(viewModelRecommendation.getDescriptionFootType(footType: footType.rawValue, brand: viewModelRecommendation.brandSelected))
                    .foregroundColor(Color.white)
                    .font(.system(size: 17))
                
                Divider()
                    .background(Color.white)
                
                HStack {
                    Text("EU")
                        .font(.system(size: 23))
                        .fontWeight(.heavy)
                    Text(viewModelRecommendation.getEUSize(brand: viewModelRecommendation.brandSelected, gender: viewModelRecommendation.genderSelected, footSize: footSize))
                        .font(.system(size: 23))
                    Spacer()
                    Text("UK")
                        .font(.system(size: 23))
                        .fontWeight(.heavy)
                    Text(viewModelRecommendation.getUKSize(brand: viewModelRecommendation.brandSelected, gender: viewModelRecommendation.genderSelected, footSize: footSize))
                        .font(.system(size: 23))
                    Spacer()
                    Text("US")
                        .font(.system(size: 23))
                        .fontWeight(.heavy)
                    Text(viewModelRecommendation.getUSSize(brand: viewModelRecommendation.brandSelected, gender: viewModelRecommendation.genderSelected, footSize: footSize))
                        .font(.system(size: 23))
                }
                
                Button(action: {
                    withAnimation {
                        showBrandPicker.toggle()
                    }
                }) {
                    HStack {
                        Text(viewModelRecommendation.brandSelected)
                            .foregroundColor(Color("PickerColor"))
                            .fontWeight(.heavy)
                        
                        Spacer()
                        
                        Image(systemName: showBrandPicker ? "chevron.up" : "chevron.down")
                            .resizable()
                            .frame(width: 17, height: 10, alignment: .center)
                            .foregroundColor(Color("PickerColor"))
                    }
                    .padding([.all], 20)
                }
                .background(Color.gray.opacity(0.25))
                .cornerRadius(10)
                if showBrandPicker == true {
                    Picker("Select your interest", selection: $viewModelRecommendation.brandSelected) {
                        ForEach(shoesBrands, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(.wheel)
                    .frame(width: UIScreen.main.bounds.size.width - 80, height: 200)
                    .background(Color.gray.opacity(0.25))
                    .onChange(of: viewModelRecommendation.brandSelected, perform: {
                        newValue in viewModelRecommendation.updateValue(type: "brand", value: newValue)
                        withAnimation {
                            showBrandPicker.toggle()
                        }
                    })
                }
                
                Button(action: {
                    withAnimation {
                        showGenderPicker.toggle()
                    }
                }) {
                    HStack {
                        Text(viewModelRecommendation.genderSelected)
                            .foregroundColor(Color("PickerColor"))
                            .fontWeight(.heavy)
                        
                        Spacer()
                        
                        Image(systemName: showGenderPicker ? "chevron.up" : "chevron.down")
                            .resizable()
                            .frame(width: 17, height: 10, alignment: .center)
                            .foregroundColor(Color("PickerColor"))
                    }
                    .padding([.all], 20)
                }
                .background(Color.gray.opacity(0.25))
                .cornerRadius(10)
                if showGenderPicker == true {
                    Picker("Select your interest", selection: $viewModelRecommendation.genderSelected) {
                        ForEach(genders, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(.wheel)
                    .frame(width: UIScreen.main.bounds.size.width - 80, height: 150)
                    .background(Color.gray.opacity(0.25))
                    .onChange(of: viewModelRecommendation.genderSelected, perform: {
                        newValue in viewModelRecommendation.updateValue(type: "gender", value: newValue)
                        withAnimation {
                            showGenderPicker.toggle()
                        }
                    })
                }
            }
            .padding([.all], 20)
        }
        .cornerRadius(10)
        .padding([.all], 20)
        .confirmationDialog("Select a color", isPresented: $showAlertDismiss, titleVisibility: .visible) {
            Button("Red") {
                print("red")
            }
            
            Button("Green") {
                print("green")
            }
            
            Button("Blue") {
                print("blue")
            }
        }
    }
}

struct ShoesRecommendation_Previews: PreviewProvider {
    static var previews: some View {
        ShoesRecommendation(viewModelTypeFoot: TypeFootViewModel(), viewModelRecommendation: RecommendationViewModel(), footSize: 10.0, footType: TypeFoot.normal)
    }
}
