//
//  RecommendationView.swift
//  kakiku-project
//
//  Created by Taufiq Ichwanusofa on 25/06/22.
//

import SwiftUI

struct RecommendationView: View {
    @ObservedObject var viewModelRecommendation: RecommendationViewModel
    @ObservedObject var viewModelTypeFoot: TypeFootViewModel
    
    init(viewModelTypeFoot: TypeFootViewModel, viewModelRecommendation: RecommendationViewModel) {
        self.viewModelTypeFoot = viewModelTypeFoot
        self.viewModelRecommendation = viewModelRecommendation
        let navbarAppearance = UINavigationBar.appearance()
        navbarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "TitleColor")!]
        navbarAppearance.titleTextAttributes = [.foregroundColor: UIColor(named: "TitleColor")!]
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.black
                    .ignoresSafeArea()
                VStack {
                    Spacer()
                    Image("wave blue-1")
                        .resizable()
                        .scaledToFill()
                        .frame(width: UIScreen.main.bounds.size.width, height: 400, alignment: .center)
                        .edgesIgnoringSafeArea(.all)
                }
                VStack(alignment: .leading) {
                    Text("Recommendation")
                        .font(.system(size: 32, weight: .heavy))
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.leading)
                        .lineLimit(0)
                        .frame(height: 32)
                        .padding(.horizontal, 50)
                    ScrollView(showsIndicators: false) {
                        Image(viewModelRecommendation.getShoesImage(brand: viewModelRecommendation.brandSelected, footType: viewModelTypeFoot.footTypeSelected.rawValue))
                            .resizable()
                            .frame(width: 250, height: 100, alignment: .center)
                            .padding(.top, 80)
                        ShoesRecommendation(viewModelTypeFoot: viewModelTypeFoot, viewModelRecommendation: viewModelRecommendation, footSize: viewModelTypeFoot.sizeFoot, footType: viewModelTypeFoot.footTypeSelected)
                            .padding(.top, 20)
                    }
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    VStack {
                        Spacer()
                        Image("PopHide")
                        Spacer()
                    }
                }
            }
        }
        .navigationBarHidden(true)
    }
}

struct RecommendationView_Previews: PreviewProvider {
    static var previews: some View {
        RecommendationView(viewModelTypeFoot: TypeFootViewModel(), viewModelRecommendation: RecommendationViewModel())
    }
}
