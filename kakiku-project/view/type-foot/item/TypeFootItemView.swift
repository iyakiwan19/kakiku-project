//
//  TypeFootItemView.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 30/06/22.
//

import SwiftUI

struct TypeFootItemView: View {
    let typeFoot: TypeFootModel
    var body: some View {
        ZStack{
            BlurView(style: .dark )
                .opacity(0.85)
            VStack(spacing: 5) {
                Image(typeFoot.imageName)
                    .resizable()
                    .frame(width: 200, height: 220, alignment: .center)
                    .padding(.top, 25)
                Divider().background(Color.white)
                    .padding(15)
                Text(typeFoot.name.rawValue)
                    .font(.title)
                    .foregroundColor(.white)
                    .fontWeight(.bold)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.horizontal)
                Text(typeFoot.description)
                    .font(.body)
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.horizontal)
                Spacer()
                
            }
        }
        .cornerRadius(10.0)
        .padding(5)
    }
}

struct TypeFootItemView_Previews: PreviewProvider {
    static let firstTypeFoot: TypeFootModel = TypeFootModel(id: 0, name: TypeFoot.normal, imageName: "FootNormal", description: "The foot lands on the outside of the heel and then rolls inward to absorb shock and support body weight.", isSelected: false)
    static var previews: some View {
        TypeFootItemView(typeFoot: firstTypeFoot)
    }
}
