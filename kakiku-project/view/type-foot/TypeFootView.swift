//
//  TypeFootView.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 24/06/22.
//

import SwiftUI

struct TypeFootView: View {
    @ObservedObject var viewModelRecommendation: RecommendationViewModel
    @ObservedObject var viewModelTypeFoot: TypeFootViewModel
    
    init(viewModelRecommendation: RecommendationViewModel, viewModelTypeFoot: TypeFootViewModel) {
        let navbarAppearance = UINavigationBar.appearance()
        navbarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor(.white)]
        navbarAppearance.titleTextAttributes = [.foregroundColor: UIColor(.white)]
        self.viewModelTypeFoot = viewModelTypeFoot
        self.viewModelRecommendation = viewModelRecommendation
    }
    var body: some View {
        NavigationView {
            ZStack {
                Color.black
                    .ignoresSafeArea()
                VStack {
                    Image("wave type foot")
                        .resizable()
                        .scaledToFill()
                        .frame(width: UIScreen.main.bounds.size.width, height: 400, alignment: .center)
                        .edgesIgnoringSafeArea(.all)
                }
                
                VStack(alignment: .center, spacing: 20) {
                    ScrollView(.horizontal, showsIndicators: false) {
                        ScrollViewReader { value in
                            HStack {
                                ForEach(viewModelTypeFoot.dummyTypeFoot) { typeFoot in
                                    VStack (spacing: 10) {
                                        TypeFootItemView(typeFoot: typeFoot)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("ColorButton"), lineWidth: viewModelTypeFoot.dummyTypeFoot[typeFoot.id].isSelected ? 3 : 0)
                                                    .shadow(color: Color("ColorButton"), radius: 5, x: 0, y: 2)
                                            )
                                            .frame(width: 300,height: 475)
                                            .padding(.horizontal, 15)
                                            .padding(.top, 30)
                                            .id(typeFoot.id)
                                        Button {
                                            resetData()
                                            viewModelTypeFoot.dummyTypeFoot[typeFoot.id].isSelected = true
                                            viewModelTypeFoot.footTypeSelected = typeFoot.name
                                            viewModelTypeFoot.footTypeSelectedIndex = typeFoot.id
                                            viewModelTypeFoot.footTypeIsSelected = true
                                            value.scrollTo(typeFoot.id)
                                        } label: {
                                            Label("Select Button", systemImage: viewModelTypeFoot.dummyTypeFoot[typeFoot.id].isSelected ? "checkmark.circle.fill" : "circle")
                                                .labelStyle(IconOnlyLabelStyle())
                                                .font(.system(size: 35).bold())
                                                .foregroundColor(Color("ColorButton"))
                                                .cornerRadius(32)
                                                .padding(5)
                                        }
                                    }.tag(typeFoot.id)
                                    
                                }
                            }
                        }
                    }
                    
                    Spacer()
                    
                    NavigationLink(destination: RecommendationView(viewModelTypeFoot: viewModelTypeFoot, viewModelRecommendation: viewModelRecommendation), isActive: $viewModelTypeFoot.showRecommendationSheet) {
                        Button(action: {
                            viewModelTypeFoot.showRecommendationSheet = true
                        }) {
                            Text("Get Recommendation")
                                .frame(minWidth: 0, maxWidth: 250)
                                .font(.system(size: 18))
                                .padding()
                                .foregroundColor(.black)
                                .background(
                                    RoundedRectangle(cornerRadius: 10, style: .continuous).fill(!viewModelTypeFoot.footTypeIsSelected ? .gray : Color("ColorButton"))
                                )
                        }
                        
                    }.disabled(!viewModelTypeFoot.footTypeIsSelected)
                    Spacer()
                }
                .padding(.top, 50)
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    VStack {
                        Spacer()
                        Image("PopHide").padding(.top, 50)
                        
                        HStack {
                            Text("Choose shape!")
                                .font(.system(size: 32, weight: .heavy))
                                .foregroundColor(Color.white)
                                .multilineTextAlignment(.center)
                                .lineLimit(0)
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.top, 30)
                                .padding(.leading, 20)
                        }
                    }
                }
            }
        }
        .navigationBarHidden(true)
    }
    
    func resetData() {
        for i in viewModelTypeFoot.dummyTypeFoot.indices {
            viewModelTypeFoot.dummyTypeFoot[i].isSelected = false
        }
    }
}

struct TypeFootView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TypeFootView(viewModelRecommendation: RecommendationViewModel(), viewModelTypeFoot: TypeFootViewModel())
                .previewDevice("iPhone 13")
                .previewInterfaceOrientation(.portrait)
        }
    }
}
