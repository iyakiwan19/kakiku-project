//
//  TypeFootDetailView.swift
//  kakiku-project
//
//  Created by Mufti Alie Satriawan on 14/07/22.
//

import SwiftUI

struct TypeFootDetailView: View {
    @ObservedObject var viewModelRecommendation: RecommendationViewModel
    @ObservedObject var viewModelTypeFoot: TypeFootViewModel
    
    init(viewModelRecommendation: RecommendationViewModel, viewModelTypeFoot: TypeFootViewModel) {
        let navbarAppearance = UINavigationBar.appearance()
        navbarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor(.white)]
        navbarAppearance.titleTextAttributes = [.foregroundColor: UIColor(.white)]
        self.viewModelTypeFoot = viewModelTypeFoot
        self.viewModelRecommendation = viewModelRecommendation
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.black
                    .ignoresSafeArea()
                VStack {
                    Image("wave detail type foot")
                        .resizable()
                        .scaledToFill()
                        .frame(width: UIScreen.main.bounds.size.width, height: 400, alignment: .center)
                        .edgesIgnoringSafeArea(.all)
                }
                
                VStack(alignment: .center, spacing: 20) {
                    ScrollView(.horizontal, showsIndicators: false) {
                        ScrollViewReader { value in
                            HStack {
                                ForEach(viewModelTypeFoot.dummyTypeFoot) { typeFoot in
                                    VStack (spacing: 10) {
                                        TypeFootItemView(typeFoot: typeFoot)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("ColorButton"), lineWidth: viewModelTypeFoot.dummyTypeFoot[typeFoot.id].isSelected ? 3 : 0)
                                                    .shadow(color: Color("ColorButton"), radius: 5, x: 0, y: 2)
                                            )
                                            .frame(width: 300,height: 475)
                                            .padding(.horizontal, 15)
                                            .padding(.top, 30)
                                            .id(typeFoot.id)
                                        Button {
                                            resetData()
                                            viewModelTypeFoot.dummyTypeFoot[typeFoot.id].isSelected = true
                                            viewModelTypeFoot.footTypeSelected = typeFoot.name
                                            viewModelTypeFoot.footTypeSelectedIndex = typeFoot.id
                                            value.scrollTo(typeFoot.id)
                                            print("TypeFoot Detail Selected:  \(typeFoot.id)")
                                        } label: {
                                            Label("Select Button", systemImage: viewModelTypeFoot.dummyTypeFoot[typeFoot.id].isSelected ? "checkmark.circle.fill" : "circle")
                                                .labelStyle(IconOnlyLabelStyle())
                                                .font(.system(size: 35).bold())
                                                .foregroundColor(Color("ColorButton"))
                                                .cornerRadius(32)
                                        }
                                    }.tag(typeFoot.id)
                                }
                            }
                            .onAppear(){
                                value.scrollTo(viewModelTypeFoot.footTypeSelectedIndex)
                            }
                        }
                    }
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    VStack(alignment: .leading) {
                        Spacer()
                        Spacer()
                        Button {
                            viewModelTypeFoot.showTypeFootDetailSheet = false
                        } label: {
                            HStack {
                                Label("Recommendation", systemImage: "chevron.left")
                                    .font(.system(size: 12))
                                    .foregroundColor(Color("ColorButton"))
                                Text("Recommendation").foregroundColor(Color("ColorButton"))
                            }
                        }.padding(.top, 50)
                        
                        HStack {
                            Text("Foot Type")
                                .font(.system(size: 32, weight: .heavy))
                                .foregroundColor(Color.white)
                                .multilineTextAlignment(.center)
                                .lineLimit(0)
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.top, 30)
                                .padding(.leading, 20)
                        }
                    }
                }
            }
        }
        .navigationBarHidden(true)
    }
    
    func resetData() {
        for i in viewModelTypeFoot.dummyTypeFoot.indices {
            viewModelTypeFoot.dummyTypeFoot[i].isSelected = false
        }
    }
}

struct TypeFootDetailView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TypeFootDetailView(viewModelRecommendation: RecommendationViewModel(), viewModelTypeFoot: TypeFootViewModel())
                .previewDevice("iPhone 13")
                .previewInterfaceOrientation(.portrait)
        }
    }
}
