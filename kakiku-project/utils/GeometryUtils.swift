//
//  GeometryUtils.swift
//  kakiku-project
//
//  Created by Gualtiero Frigerio on 20/05/21.
//  Edited by Ricky Suprayudi on 27/06/22.
//

import Foundation
import ARKit
import SwiftUI

class GeometryUtils {   // utils langsung
    static func createDot(result: ARRaycastResult) -> SCNNode {
        let dotGeometry = SCNSphere(radius: 0.005)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.red
        
        dotGeometry.materials = [material]
        let dotNode = SCNNode(geometry: dotGeometry)
        dotNode.simdWorldTransform = result.worldTransform
        
        return dotNode
    }

    static func createRuler() -> SCNNode {
        let rulerWidth = 0.07
        let rulerHeight = 0.36
        let rulerGeometry = SCNPlane(width: rulerWidth, height: rulerHeight)
        
        let material = SCNMaterial()
        material.diffuse.contents = UIImage(named: "Ruler")
        rulerGeometry.materials = [material]
        let rulerNode = SCNNode(geometry: rulerGeometry)
        rulerNode.simdEulerAngles.x = GLKMathDegreesToRadians(-90.0)
        
        return rulerNode
    }

    static func createFoot() -> SCNNode {
        let footWidth = 0.280
        let footHeight = 0.400
        let footGeometry = SCNPlane(width: footWidth, height: footHeight)
        
        let material = SCNMaterial()
        material.diffuse.contents = UIImage(named: "FeetPad")
        footGeometry.materials = [material]
        let footNode = SCNNode(geometry: footGeometry)
        footNode.simdEulerAngles.x = GLKMathDegreesToRadians(-90.0)
        
        return footNode
    }

    static func createBorder() -> SCNNode {
        let borderWidth = 0.25
        let borderHeight = 0.005
        let borderLength = 0.001
        let borderChamfer = 0.03
        let borderGeometry = SCNBox(width: borderWidth, height: borderHeight, length: borderLength, chamferRadius: borderChamfer)
        
        let material = SCNMaterial()
        material.diffuse.contents = UIColor(Color("UndoAndMarkButton"))
        borderGeometry.materials = [material]
        let borderNode = SCNNode(geometry: borderGeometry)
        
        return borderNode
    }

    static func createText(text: Any) -> SCNNode {
        let textGeometry = SCNText(string: "\(text) cm", extrusionDepth: 1.0)
        textGeometry.firstMaterial?.diffuse.contents = UIColor.white
        textGeometry.extrusionDepth = 0.0
        
        let textNode = SCNNode(geometry: textGeometry)
        textNode.scale = SCNVector3(0.0020, 0.0020, 0.0020)
        let (minVec, maxVec) = textNode.boundingBox
        textNode.pivot = SCNMatrix4MakeTranslation((maxVec.x - minVec.x) / 2 + minVec.x, (maxVec.y - minVec.y) / 2 + minVec.y, 0)
        textNode.simdEulerAngles.x = GLKMathDegreesToRadians(-90.0)
        
        return textNode
    }
    
    static func createCurtain(width: CGFloat, height: CGFloat, color: UIColor) -> SCNNode {
        let curtainWidth = width
        let curtainHeight = height
        let curtainGeometry = SCNPlane(width: curtainWidth, height: curtainHeight)
        
        let material = SCNMaterial()
        material.diffuse.contents = color
        curtainGeometry.materials = [material]
        let curtainNode = SCNNode(geometry: curtainGeometry)
        curtainNode.simdEulerAngles.x = GLKMathDegreesToRadians(-90.0)
        
        return curtainNode
    }


}
