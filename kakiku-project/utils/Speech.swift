//
//  Speech.swift
//  kakiku-project
//
//  Created by Taufiq Ichwanusofa on 24/06/22.
//

import Foundation
import AVKit

enum EnumDirection {
    case closer, away, trackingLimited, trackingReady, trackingNotAvailable, voiceEnabled, voiceDisabled, measureReady, measureDone, firstPoint, secondPoint
}

class Speech {
    func doSpeak(with input: EnumDirection) {
        var textToSpeech = ""

        switch input {
        case .closer:
            textToSpeech = "move closer"
        case .away:
            textToSpeech = "move away"
        case .trackingLimited:
            textToSpeech = "tracking limited"
        case .trackingReady:
            textToSpeech = "tracking ready"
        case .trackingNotAvailable:
            textToSpeech = "tracking not available"
        case .voiceEnabled:
            textToSpeech = "voice enabled"
        case .voiceDisabled:
            textToSpeech = "voice disabled"
        case .measureReady:
            textToSpeech = "measurement ready"
        case .measureDone:
            textToSpeech = "measurement done"
        case .firstPoint:
            textToSpeech = "position tracking dot into back of the heel"
        case .secondPoint:
            textToSpeech = "position tracking dot into the longest finger"
        }

        let utterance = AVSpeechUtterance(string: textToSpeech)
        utterance.rate = 0.4
        utterance.pitchMultiplier = 0.8
        utterance.postUtteranceDelay = 0.2
        utterance.volume = 0.8
        
        let voice = AVSpeechSynthesisVoice(language: "en-US")
        utterance.voice = voice
        
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
    }
}

